<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@home');

Route::get('/about', 'PagesController@about');

Route::get('/buku', 'BukuController@index');

Route::get('/buku/create/', 'BukuController@create');

Route::post('/buku', 'BukuController@store');


Route::get('/buku/edit/{id}/', 'BukuController@edit');

Route::post('/buku/update/','BukuController@update');

Route::get('/buku/delete/{id}', 'BukuController@Delete');