<?php

namespace App\Http\Controllers;

use CreateBooksTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    public function index()
    {
        //$buku = DB::table('books')->get();

        $buku = \App\Book::paginate(5);
        return view('buku.index', ['mahasiswa'=> $buku]);
    }

    public function create()
    {
        return view('buku.create');
    }

    public function store(Request $request)
    {
        DB::table('books')->insert([
            'nama' => $request->nama,
            'penerbit' => $request->penerbit,
        ]);
        return redirect('/buku');
    }

    public function show($id)
    {
        return view('buku.index', compact('mahasiswa'));
    }

    public function edit($id)
    {
        $books = DB::table('books')->where('id',$id)->get();
        return view('buku.edit',['books' => $books]); 
    }

   
    public function update(Request $request)
    {
    DB::table('books')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'penerbit' => $request->penerbit,
        ]);
       return redirect('/buku');

       
    }

    public function delete($id)
    {
        DB::table('books')->where('id',$id)->delete();
       return redirect('/buku');
    }
}
