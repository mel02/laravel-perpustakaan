@extends('layout/main')

@section('title', 'Tambah Data Buku')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-3">Tambah Data Buku</h1>

            <form method="POST" action="/buku">
            @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama">
                </div>
                <div class="form-group">
                    <label for="penerbit">Penerbit</label>
                    <input type="text" class="form-control" id="penerbit" placeholder="Masukkan Penerbit" name="penerbit">
                </div>

                <button type="submit" clas="btn btn-primary">Tambah Data</button>
            </form>
        </div>
    </div>
</div>
@endsection