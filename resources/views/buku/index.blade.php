@extends('layout/main')

@section('title', 'Daftar Buku')


@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Daftar Buku</h1>

            <a href="/buku/create" class="btn btn-primary my-3">Tambah Data Buku</a>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Penerbit</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($mahasiswa as $mhs)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $mhs->nama }}</td>
                        <td>{{ $mhs->penerbit }}</td>
                        <td>
                            <a href="/buku/edit/{{ $mhs->id }}" >Edit</a>

                            <a onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')"href="/buku/delete/{{ $mhs->id }}" >Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
   {{$mahasiswa->links()}}
</div>
@endsection