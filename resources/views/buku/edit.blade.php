@extends('layout/main')

@section('title', 'Edit Buku')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-3">Edit Buku</h1>

            @foreach($books as $b)
            <form method="POST" action="/buku/update">
            @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama" value="{{ $b->nama}}">
                </div>
                <div class="form-group">
                    <label for="penerbit">Penerbit</label>
                    <input type="text" class="form-control" id="penerbit" placeholder="Masukkan Penerbit" name="penerbit" value="{{ $b->penerbit}}">
                </div>

                <button type="submit" clas="btn btn-primary">Update</button>
            </form>
            @endforeach
        </div>
    </div>
</div>
@endsection