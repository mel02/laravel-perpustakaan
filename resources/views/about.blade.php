@extends('layout.main')

@section('title', 'About')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
            <h1 class="mt-3">Perpustakaan merupakan suatu tempat untuk belajar, mencari dan mengembangkan informasi ataupun lembaga pendidikan, dan juga sebagai saranan edukatif dalam pendidikan yang dikelola sedemikian rupa</h1>
            </div>
        </div>
    </div>
@endsection